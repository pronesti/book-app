import React from 'react';
import { iconDelete, iconEdit } from '../icons';

function BookControls(props) {
    return (
        <td>
            <span onClick={() => props.handleEdit(props.book)}>{iconEdit}</span>
            <span onClick={() => props.deleteBook(props.book.id)}>{iconDelete}</span>
        </td>
    )
}

export default BookControls;