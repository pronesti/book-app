import { Table } from 'react-bootstrap';
import BookRow from './BookRow';

function BookTable(props) {
    return (
        <Table responsive>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Category</th>
                </tr>
            </thead>

            <tbody> 
            { props.books && 
               props.books.map(b => <BookRow key={b.id} book={b} deleteBook={props.deleteBook} handleEdit={props.handleEdit} />)
            }
            </tbody>    
        </Table>
    );
}

export default BookTable;