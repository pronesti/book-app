import { Modal, Form, Button } from "react-bootstrap";
import { useState } from 'react';
import uuid from 'react-uuid'


const BookForm = (props) => {
    const [title, setTitle] = useState(props.book ? props.book.title : '');
    const [author, setAuthor] = useState(props.book ? props.book.author : '');
    const [category, setCategory] = useState(props.book ? props.book.category : '');

    const [submitted, setSubmitted] = useState(false);
    const [errorMessage, setErrorMessage] = useState();

    const handleSumbit = (event) => {
        event.preventDefault();
    
        // VALIDATION 
        if (title.trim() !== '' && 
            author.trim() !== '' &&
            category.trim() !== ''
        ) 
        {
          const book = { 
            // if undefined, it means we are creating a task
            // addOrEdit will take care of generating the id from API
            id: props.book ? props.book.id : uuid(), 
            title: title, 
            author: author, 
            category: category
          }; 
    
          props.addOrEditBook(book);
          setSubmitted(true);
          props.handleClose();
        } else
          setErrorMessage('Please fill in all the required fields');
    }

    return (
        <Modal show={!submitted} size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
            <Modal.Header className="bg-primary">
                <Modal.Title>{ props.book ? "Edit" : "Add"} Book</Modal.Title>
            </Modal.Header>

            <Form>
                <Modal.Body>
                    <Form.Group>
                        <Form.Label>Title</Form.Label>
                        <Form.Control type='text' value={title} onChange={ev => setTitle(ev.target.value)}></Form.Control>
                        <span style={{ color: 'red' }}>{errorMessage}</span>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Author</Form.Label>
                        <Form.Control type='text' value={author} onChange={ev => setAuthor(ev.target.value)}></Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Category</Form.Label>
                        <Form.Control type='text' value={category} onChange={ev => setCategory(ev.target.value)}></Form.Control>
                    </Form.Group>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant='primary' onClick={handleSumbit} >Save</Button>
                    <Button variant='secondary' onClick={props.handleClose}>Cancel</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    );

}

export default BookForm;