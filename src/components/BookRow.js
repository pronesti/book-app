import BookControls from './BookControls';

const BookRow = (props) => {
    return (
        <tr>
            <td>{props.book.id}</td>
            <td>{props.book.title}</td>
            <td>{props.book.author}</td>
            <td>{props.book.category}</td>
            <BookControls book={props.book} 
                              deleteBook={props.deleteBook} 
                              handleEdit={props.handleEdit}
             />                 
        </tr>
    );
}



export default BookRow;