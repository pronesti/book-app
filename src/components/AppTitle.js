import { Col } from 'react-bootstrap';

function AppTitle() {
  return (
      <Col>
        <h1>Available Books</h1>
      </Col>
  );
}

export default AppTitle;