import Book from './Book';

const APIendpoint = 'https://a8gunlrt27.execute-api.us-east-1.amazonaws.com/';
const url = APIendpoint + '/books';

async function getBooks() {
    const response = await fetch(url);
    const bookJson = await response.json();

    if (response.ok) {
        return bookJson.Items.map(b => new Book(b.id, b.title, b.author, b.category));
    } else {
        throw bookJson;
    }
}

async function deleteBook(id) {
    return new Promise((resolve, reject) => {
        fetch(url + '/' + id, {
            method: 'DELETE',
            mode: 'cors'
        }).then( response => {
            if(response.ok) 
                resolve(null);
        }).catch( err => reject({'err': 'DELETE error'})); // connection errors
    });
}

async function addBook(book){
    const response = await fetch(url, {
        method: 'PUT',
        mode: 'cors',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(book)
    });

    return response.ok ? null : { 'err' : 'PUT error'};
}

async function updateBook(book){
    const response = await fetch(url, {
        method: 'PUT',
        mode: 'cors',
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify(book)
    });

    return response.ok ? null : { 'err': 'PUT error' };
}

const API = { getBooks, deleteBook, addBook, updateBook};
export default API;