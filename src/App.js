import React, { useState, useEffect } from 'react';
import BookTable from './components/BookTable';
import AppTitle from './components/AppTitle';
import BookForm from './components/BookForm';
import API from './api/API';
import { Button, Container, Row } from 'react-bootstrap';

import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

function App() {
  const [books, setBooks] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [loading, setLoading ] = useState(true);
  // state to fill BookForm when rendered in EDIT mode
  const [selectedBook, setSelectedBook] = useState();

  // mount
  useEffect(() => {
    API.getBooks()
      .then(bks => {
        setBooks(bks);
        setLoading(false);
      })
      .catch();
  }, []);

  /**
   * 
   * @param {*} book 
   */
  const addOrEditBook = (book) => {
    if(book.id) { // edit (the book already has the id)
      API.updateBook(book)
         .then(() => {
           API.getBooks().then((bks) => setBooks(bks))
         })
         .catch();

    } else {
      API.addBook(book)
          .then(() => {
            API.getBooks().then((bks) => setBooks(bks))
          })
          .catch();
    }
  }

  /**
   * 
   * @param {*} id 
   */
  const deleteBook = (id) => {
    API.deleteBook(id)
      .then(() => {
        API.getBooks().then(bks => {
          setBooks(bks);
        })
      })
      .catch();
  }

  const handleEdit = (book) => {
    setSelectedBook(book);
    setShowModal(true);
  }

  const handleClose = () => {
    setSelectedBook();
    setShowModal(false);
  }


  return (
    <Container className="App">
      <Row>
        <AppTitle/>
      </Row>

      <Row>
        {loading ? <span>🕗 Please wait, loading your books... 🕗</span> 
        :
        <BookTable books={books} deleteBook={deleteBook} handleEdit={handleEdit}/>
      }
      </Row>
      {
        showModal && <BookForm addOrEditBook={addOrEditBook} book={selectedBook} handleClose={handleClose}/>
      }
      <Button variant='primary' onClick={() => setShowModal(true)}>Add</Button>
    </Container>
  );
}

export default App;
