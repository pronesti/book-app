# Book App

This simple react application allows to add, edit or delete books employing DynamoDB and a serverless lambda function.

## Usage

First istall the required dependencies 
```
npm install 
```

then run the application
```
npm start
```

A new tab in your browser will appear. If it won't, simply open yourself a new one and type `http://localhost:3000/`.